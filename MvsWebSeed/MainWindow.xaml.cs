﻿using System;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;

namespace MvsWebSeed
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ICommand ShowTestWindow { get; private set; }

        public MainWindow()
        {
            InitializeComponent();

            Init();

            this.DataContext = this;
        }
        
        private void Init()
        {
            ShowTestWindow = new DelegateCommand(() =>
            {
#if !DEBUG
                try
                {
#endif
                    var win = new TestWindow();
                    win.ShowDialog();
#if !DEBUG
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
#endif
            });
        }
    }
}