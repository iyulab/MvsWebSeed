﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Mvs.Core;
using Newtonsoft.Json;
using CefSharp;
using Prism.Commands;
using System.Collections;
using Mvs.Controls;
using System.Configuration;

namespace MvsWebSeed
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        #region dependency properties...

        public IEnumerable<ITool> Tools { get { return (IEnumerable<ITool>)GetValue(ToolsProperty); } set { SetValue(ToolsProperty, value); } }
        public ITool SelectedTool { get { return (ITool)GetValue(SelectedToolProperty); } set { SetValue(SelectedToolProperty, value); } }
        public IComponent SelectedComponent { get { return (IComponent)GetValue(SelectedComponentProperty); } set { SetValue(SelectedComponentProperty, value); } }
        
        public static readonly DependencyProperty ToolsProperty = DependencyProperty.Register("Tools", typeof(IEnumerable<ITool>), typeof(TestWindow), new PropertyMetadata());
        public static readonly DependencyProperty SelectedToolProperty = DependencyProperty.Register("SelectedTool", typeof(ITool), typeof(TestWindow), new PropertyMetadata());
        public static readonly DependencyProperty SelectedComponentProperty = DependencyProperty.Register("SelectedComponent", typeof(IComponent), typeof(TestWindow), new PropertyMetadata());
        
        #endregion

        #region properties...

        public ICommand ShowDevToolsCommand { get; private set; }
        public ICommand AddComponentCommand { get; private set; }
        public ICommand UpdateComponentsCommand { get; private set; }

        #endregion

        #region fields...

        private LocalConfig config;

        #endregion

        public TestWindow()
        {
            InitializeComponent();

            config = new LocalConfig("app.config");

            Init();
            this.DataContext = this;
        }

        private void Init()
        {
            ShowDevToolsCommand = new DelegateCommand(() =>
            {
                this.browser.ShowDevTools();
            });

            AddComponentCommand = new DelegateCommand(() =>
            {
                if (this.SelectedTool == null) return;
                AddComponent(this.SelectedTool);
            });

            UpdateComponentsCommand = new DelegateCommand(async () =>
            {
                var serverAddress = config.Get("ServerAddress", "http://localhost:3000");
                await Mvs.Utils.Facade.UpdateWebToolsAsync(serverAddress);
                Tools = Mvs.Utils.Facade.LoadWebTools().ToArray();
            });
            
            Tools = Mvs.Utils.Facade.LoadWebTools().ToArray();

            MvsSynchronousEvent<GetPropertiesEventArgs>.Instance.Subscribe(OnGetProperties);
            MvsSynchronousEvent<ComponentPropertyChangedEventArgs>.Instance.Subscribe(OnComponentPropertyChanged);
        }
        
        private void OnGetProperties(GetPropertiesEventArgs e)
        {   
            var properties = browser.GetComponentProperties(e.Component).ToArray();

            var tool = Tools.Where(p => p.Id == e.Component.Name).SingleOrDefault();
            if (tool.Properties != null)
            {
                foreach (var property in properties)
                {
                    var pInfo = tool.Properties.Where(p => p["name"].ToString() == property.Name).SingleOrDefault();
                    if (pInfo == null) continue;

                    var editor = pInfo["editor"].ToString();
                    property.Editor = editor;
                }
            }

            e.Properties = properties;
        }

        private void OnComponentPropertyChanged(ComponentPropertyChangedEventArgs e)
        {
            this.browser.ApplyPropertyValue(this.SelectedComponent.Id, e.Name, e.Value);
        }

        private void AddComponent(ITool tool)
        {
            var component = tool.GetComponent();
            this.browser.Components.Add(component);
        }
    }
}