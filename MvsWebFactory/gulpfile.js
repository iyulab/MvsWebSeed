﻿"use strict";

var gulp = require('gulp');
var stylus = require("gulp-stylus");
var fs = require('fs');
var path = require("path");
var through = require('through2');
var rename = require('gulp-rename');
var es = require('event-stream');
var ts = require('gulp-typescript');

var paths = {
    libs: './libs/',
    root: './wwwroot/'
};

var tsProject = ts.createProject('tsconfig.json');

function func_stylus(path) {
    return gulp.src(path)
        .pipe(stylus({
            //compress: true,
            //'include css': true,
            //use: [typographic(), nib(), rupture(), jeet(), commStyl()],
            //import: [paths.stylus]
        }))
        .pipe(gulp.dest(function (file) { return file.base; }));
}

gulp.task('stylus', function () {
    func_stylus(paths.libs + "**/*.styl");
});

gulp.task('ts', function () {
    gulp.src(paths.libs + "**/*.ts")
        .pipe(ts(tsProject))
        .pipe(gulp.dest(function (file) { return file.base; }));
});

gulp.task('js', function () {
    gulp.src([paths.libs + "**/*.js", "!" + paths.libs + "components/**/*.*"])
        .pipe(gulp.dest(paths.root));
});

gulp.task('css', function () {
    gulp.src([paths.libs + "**/*.css", "!" + paths.libs + "components/**/*.*"])
        .pipe(gulp.dest(paths.root));
});

gulp.task('html', function () {
    gulp.src([paths.libs + "**/*.html", "!" + paths.libs + "components/**/*.*"])
        .pipe(gulp.dest(paths.root));
});

gulp.task('json', function () {
    gulp.src(paths.libs + "**/*.json")
        .pipe(gulp.dest(paths.root));
});

function getFolders(dir) {
    return fs.readdirSync(dir)
      .filter(function (file) {
          return fs.statSync(path.join(dir, file)).isDirectory();
      });
}

function packComponent() {
    var action = function (file) {
        var text = file.contents.toString()        
        try {
            var obj = JSON.parse(text);
            var path = file.path.substr(0, file.path.lastIndexOf('\\') + 1);
            var shell = fs.readFileSync(path + 'shell.html', 'UTF-8');
            var style = fs.readFileSync(path + 'style.css', 'UTF-8');
            var template = fs.readFileSync(path + 'template.html', 'UTF-8');
            var script = fs.readFileSync(path + 'script.js', 'UTF-8');
            text = shell + '<dom-module id=' + obj.id + '><style>' + style + '</style><template>' + template + '</template><script>' + script + '</script></dom-module>';
        } catch (e) {
            console.log("packComponent Error: " + e.message);
        }
        file.contents = new Buffer(text);

        return file;
    }

    return through.obj(function (file, encoding, callback) {
        if (file.isNull()) return callback(null, file);        
        callback(null, action(file));
    });
}

gulp.task('components', function () {
    var folders = getFolders(paths.libs + "components");
    var tasks = folders.map(function (folder) {
        if (fs.existsSync(paths.libs + "components/" + folder + "/component.html")) {
            return gulp.src(paths.libs + "components/" + folder + "/component.html")
                .pipe(gulp.dest(paths.root + "components/" + folder));
        }
        else {
            return gulp.src(paths.libs + "components/" + folder + "/tool.json")
                .pipe(packComponent())
                .pipe(rename(function (path) {
                    path.basename = 'component';
                    path.extname = '.html';
                }))
                .pipe(gulp.dest(paths.root + "components/" + folder));
        }
    });

    return es.concat.apply(null, tasks);
});

gulp.task('build', [
    "ts", "stylus", "js", "css", "html", "json", "components"
]);

gulp.task('watch', function () {
    gulp.watch([paths.libs + "**/*.ts"], ['ts']);
    gulp.watch([paths.libs + "**/*.styl"], ['stylus']);
    gulp.watch([paths.libs + "**/*.js"], ['js']);
    gulp.watch([paths.libs + "**/*.css"], ['css']);
    gulp.watch([paths.libs + "**/*.html"], ['html']);
    gulp.watch([paths.libs + "**/*.json"], ['json']);
    gulp.watch([paths.libs + "components/**/script.js", paths.libs + "components/**/style.css", paths.libs + "components/**/template.html", paths.libs + "components/**/shell.html"], ['components']);
});