﻿/// <reference path="../../typings/_references.d.ts" />
declare var bound;

function addComponent(meta: string, isReady: string) {

    var data = JSON.parse(meta);
    var toolName: string = data.ToolName;
    var name: string = data.Name;
    var id: string = data.Id;
    
    if (document.getElementById("component-" + toolName) == null) {
        var path = `/components/${toolName}/component.html`;
        var link: HTMLLinkElement = document.createElement('link');
        link.setAttribute('designed', '');
        link.rel = "import";
        link.href = path;
        link.id = "component-" + toolName;
        document.head.appendChild(link);
    }

    var element = document.createElement(name);
    element.setAttribute('designed', '');
    element.className = "element";
    element.id = id;

    var properties: Array<{ Name: string, Value: Object}> = data.Properties;
    properties.forEach((v, i, a) => {
        try {
            element[v.Name] = v.Value;
        } catch (e) {
            console.log(e);
        }
    });

    var loadedAction = function() {
        if (eval(isReady.toLowerCase()) == true) {
            (<any>element).createNew();
        }        
    }

    var $rootCanvas = $('.root.canvas');
    $rootCanvas.append(element);
    if (element.innerHTML.length < 1)
    {
        var listener = function() {
            element.removeEventListener('DOMSubtreeModified', listener)
            loadedAction();        
            bound.onLoadedComponent(id);
        };
        element.addEventListener('DOMSubtreeModified', listener);
    }    
    else {
        loadedAction();
        bound.onLoadedComponent(id);        
    }
}

function removeComponent(id: string) {
    
    var element = document.getElementById(id);
    $(element).remove();
}

function getProperties(id: string) {
        
    var element = document.getElementById(id);
    
    var result = new Array<{ type: string, name: string, value: any, valueType: string, display: string, group: string, editor: string }>();    

    var properties = (<any>element).properties;
    Object.getOwnPropertyNames(properties).forEach(name => {
        result.push({ 
            type: properties[name].type.name, 
            name: name, 
            value: element[name],
            valueType: properties[name]['valueType'],
            display: properties[name]['display'],
            group: properties[name]['group'], 
            editor: properties[name]['editor']
        });
    });
    
    (<any>element).behaviors.forEach((v, i, a) => {
        var properties = v.properties;
        Object.getOwnPropertyNames(v.properties).forEach(name => {
            result.push({ 
                type: properties[name].type.name, 
                name: name, 
                value: element[name],
                valueType: properties[name]['valueType'],
                display: properties[name]['display'],
                group: properties[name]['group'], 
                editor: properties[name]['editor']
            });
        });
    });
    
    return result;
}

function applyPropertyValue(target: string, name: string, value: string) {

    var element = <Element>document.getElementById(target);

    try {
        element[name] = JSON.parse(value);
    } catch (e) {
        try
        {
            element[name] = value;
        } catch (error) { }        
    }

    console.log(`applyPropertyValue - ${target}, ${name} : ${value}`);
}

function applyStyle(target: string, name: string, value: string) {

    var element = document.querySelector(target);
    
    try {
        $(element).css(name, JSON.parse(value));
    } catch (e) {
        try
        {
            $(element).css(name, value);
        } catch (error) { }
    }

    console.log(`applyStyle - ${name} : ${value}`);
}