/// <reference path="../../typings/_references.d.ts" />

namespace Mvs {
    
    export interface ILayout {
        top?: string;
        left?: string;
        width?: string;
        height?: string;
        margin?: string;
        padding?: string;
    }

    export interface IAppearance {
        background?: string;
    }

    export interface IBorder {
        border?: string;
        borderStyle?: string;
        borderWidth?: string;
        borderRadius?: string;
    }
    
    export interface IFont {
        color?: string;
        fontFamily?: string;
        fontSize?: string;
        fontStyle?: string;
        fontWeight?: string;
    }

    class Convert {
        static ToPixel(value: any): string {
            if (value == 0) return '0';                        
            return  isNaN(value) ? value : value + "px";
        }
    }
    
    export class ComponentBaseBehavior extends polymer.Base {

        @property({ type: Object, value: { top: 'auto', left: 'auto', width: 'initial', height: 'initial', margin: '0' }, display: "Layout", group: "Layout", valueType: 'Layout' })
        layout: Object;
        @observe("layout")
        layoutChanged(newValue, oldValue) {
            var data: ILayout = newValue;
            if (data[0] == "{") data = JSON.parse(newValue);
            
            if (data.top != undefined) this.customStyle['--top'] = Convert.ToPixel(data.top);
            if (data.left != undefined) this.customStyle['--left'] = Convert.ToPixel(data.left);
            if (data.width != undefined) this.customStyle['--width'] = Convert.ToPixel(data.width);
            if (data.height != undefined) this.customStyle['--height'] = Convert.ToPixel(data.height);
            if (data.margin != undefined) this.customStyle['--margin'] = Convert.ToPixel(data.margin);
            if (data.padding != undefined) this.customStyle['--root-margin'] = Convert.ToPixel(data.padding);
            this.updateStyles();
        }

        @property({ type: Object, value: { background: 'transparent' }, display: "Appearance", group: "Appearance", valueType: 'Appearance' })
        appearance: Object;
        @observe("appearance")
        appearanceChanged(newValue, oldValue) {
            var data: IAppearance = newValue;
            if (data[0] == "{") data = JSON.parse(newValue);

            if (data.background != undefined) this.customStyle['--background'] = data.background;
            this.updateStyles();
        }

        @property({ type: Object, value: { border: 'black', borderStyle: 'solid', borderWidth: '0', borderRadius: 'initial' }, display: "Border", group: "Layout", valueType: 'Border' })
        border: Object;
        @observe("border")
        borderChanged(newValue, oldValue) {
            var data: IBorder = newValue;
            if (data[0] == "{") data = JSON.parse(newValue);

            if (data.border != undefined) this.customStyle['--border'] = data.border;
            if (data.borderStyle != undefined) this.customStyle['--border-style'] = data.borderStyle;
            if (data.borderWidth != undefined) this.customStyle['--border-width'] = data.borderWidth;
            if (data.borderRadius != undefined) this.customStyle['--border-radius'] = data.borderRadius;
            this.updateStyles();
        }

        @property({ type: Object, value: { fontFamily: 'Sans-serif', fontSize: 'initial', fontStyle: 'normal', fontWeight: 'normal' }, display: "Font", group: "Appearance", valueType: 'Font' })
        font: Object;
        @observe("font")
        fontSizeChanged(newValue, oldValue) {
            var data: Mvs.IFont = newValue;
            if (data[0] == "{") data = JSON.parse(newValue);

            if (data.color != undefined) this.customStyle['--color'] = data.color;
            if (data.fontFamily != undefined) this.customStyle['--font-family'] = data.fontFamily;
            if (data.fontSize != undefined) this.customStyle['--font-size'] = data.fontSize;
            if (data.fontStyle != undefined) this.customStyle['--font-style'] = data.fontStyle;
            if (data.fontWeight != undefined) this.customStyle['--font-weight'] = data.fontWeight;
            this.updateStyles();
        }
    }
    
    export abstract class ComponentBase extends polymer.Base {
        abstract createNew();                
    }    
}