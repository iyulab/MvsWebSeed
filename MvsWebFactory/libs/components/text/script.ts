﻿/// <reference path="../../../typings/_references.d.ts" />

@component("mvs-text")
@behavior(Mvs.ComponentBaseBehavior)
class MvsText extends Mvs.ComponentBase {

    createNew() {
        var _this: any = this;
        _this.appearance = { background: "skyblue" };                    
    }

    @property({ type: String, value: "Text", display: "Text", group: "feature" })
    text: string;
}

MvsText.register();