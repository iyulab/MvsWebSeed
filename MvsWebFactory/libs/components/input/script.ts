﻿/// <reference path="../../../typings/_references.d.ts" />

@component("mvs-input")
@behavior(Mvs.ComponentBaseBehavior)
class MvsInput extends Mvs.ComponentBase {

    createNew() {        
    }
    
    @property({ type: Boolean, value: false, display: "Multiline", group: "feature" })
    multiline: boolean;
}

MvsInput.register();