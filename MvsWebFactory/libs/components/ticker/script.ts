﻿/// <reference path="../../../typings/_references.d.ts" />

@component("mvs-ticker")
@behavior(Mvs.ComponentBaseBehavior)
class MvsTicker extends Mvs.ComponentBase {

    createNew() {
    }
    
    animate() {
        console.log('animate');
        var $ticker = this.$.ticker;
        var $firstItem = $($ticker).children('li:first');
        $firstItem.css("transition", `transform ${this.duration}ms`);
        $firstItem[0].addEventListener('webkitTransitionEnd', function(event) {
            $firstItem.remove();          
            $firstItem.css("transform", "translateX(0%)");  
            $firstItem.css("transform", "translateY(0%)");
            $($ticker).append($firstItem);
            
            if ($ticker.children[0].tagName == 'TEMPLATE') {
                var template = $ticker.children[0];
                $(template).remove();
                $($ticker).append(template);
            }
        }, false );

        if (this.direction == 'LeftToRight') {
            $firstItem.css("transform", "translateX(100%)");
        } else if (this.direction == 'RightToLeft') {
            $firstItem.css("transform", "translateX(-100%)");
        } else if (this.direction == 'TopToBottom') {
            $firstItem.css("transform", "translateY(100%)");
        } else if (this.direction == 'BottomToTop') {
            $firstItem.css("transform", "translateY(-100%)");
        }
    }
    
    @property({ type: Array, value: [ "dummy message #1", "dummy message #2", "dummy message #3", "dummy message #4", "dummy message #5" ], display: "Message", group: "feature", valueType: "Array<string>" })
    messages: string;
    
    animateFunction;
    @property({ type: Number, value: 1000 })
    speed: number;    
    @observe("speed")
    speedChanged(newValue, oldValue) {
        
        clearInterval(this.animateFunction);                
        this.animateFunction = setInterval(() => {
            this.animate();
        }, newValue);
    }
    
    @property({ type: String, value: 'RightToLeft', display: "Direction", group: "feature" })
    direction: string;
    
    @property({ type: Number, value: 500, group: "feature" })
    duration: number;
}

MvsTicker.register();