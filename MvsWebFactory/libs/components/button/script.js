/// <reference path="../../../typings/_references.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var MvsButton = (function (_super) {
    __extends(MvsButton, _super);
    function MvsButton() {
        _super.apply(this, arguments);
    }
    MvsButton.prototype.createNew = function () {
    };
    MvsButton.prototype.boxStyleChanged = function (newValue, oldValue) {
        if (newValue == "raised") {
            $(this.$.button).attr("raised", "");
        }
        else {
            $(this.$.button).removeAttr("raised");
        }
    };
    MvsButton.prototype.inkEffectChanged = function (newValue, oldValue) {
        if (newValue == true) {
            $(this.$.button).removeAttr("noink");
        }
        else {
            $(this.$.button).attr("noink", "");
        }
    };
    MvsButton.prototype.isEnabledChanged = function (newValue, oldValue) {
        if (newValue == true) {
            $(this.$.button).removeAttr("disabled");
        }
        else {
            $(this.$.button).attr("disabled", "");
        }
    };
    __decorate([
        property({ type: String, value: "Submit", display: "Text", group: "feature" })
    ], MvsButton.prototype, "text", void 0);
    __decorate([
        property({ type: String, value: "", display: "BoxStyle", group: "feature", editor: "combobox", sources: ["nothing", "raised"] })
    ], MvsButton.prototype, "boxStyle", void 0);
    __decorate([
        observe("boxStyle")
    ], MvsButton.prototype, "boxStyleChanged", null);
    __decorate([
        property({ type: Boolean, value: true, display: "InkEffect", group: "feature", description: '클릭할 때 물결효과가 나타납니다.' })
    ], MvsButton.prototype, "inkEffect", void 0);
    __decorate([
        observe("inkEffect")
    ], MvsButton.prototype, "inkEffectChanged", null);
    __decorate([
        property({ type: Boolean, value: true, display: "IsEnabled", group: "feature" })
    ], MvsButton.prototype, "isEnabled", void 0);
    __decorate([
        observe("isEnabled")
    ], MvsButton.prototype, "isEnabledChanged", null);
    MvsButton = __decorate([
        /// <reference path="../../../typings/_references.d.ts" />
        component("mvs-button"),
        behavior(Mvs.ComponentBaseBehavior)
    ], MvsButton);
    return MvsButton;
}(Mvs.ComponentBase));
MvsButton.register();
