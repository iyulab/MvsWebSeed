﻿/// <reference path="../../../typings/_references.d.ts" />

@component("mvs-button")
@behavior(Mvs.ComponentBaseBehavior)
class MvsButton extends Mvs.ComponentBase {

    createNew() {        
    }
    
    @property({ type: String, value: "Submit", display: "Text", group: "feature" })
    text: string;    
    
    @property({ type: String, value: "", display: "BoxStyle", group: "feature", editor: "combobox", sources: [ "nothing", "raised" ] })
    boxStyle : string;
    @observe("boxStyle")
    boxStyleChanged(newValue, oldValue) {
        if (newValue == "raised"){
            $(this.$.button).attr("raised", "");
        } else {
            $(this.$.button).removeAttr("raised");
        }
    }
    
    @property({ type: Boolean, value: true, display: "InkEffect", group: "feature", description: '클릭할 때 물결효과가 나타납니다.' })
    inkEffect : boolean;
    @observe("inkEffect")
    inkEffectChanged(newValue, oldValue) {
        if (newValue == true){
            $(this.$.button).removeAttr("noink");
        } else {
            $(this.$.button).attr("noink", "");
        }
    }
    
    @property({ type: Boolean, value: true, display: "IsEnabled", group: "feature" })
    isEnabled : boolean;
    @observe("isEnabled")
    isEnabledChanged(newValue, oldValue) {
        if (newValue == true){
            $(this.$.button).removeAttr("disabled");
        } else {
            $(this.$.button).attr("disabled", "");
        }
    }    
}

MvsButton.register();