/// <reference path="../../typings/_references.d.ts" />
function addComponent(meta, isReady) {
    var data = JSON.parse(meta);
    var toolName = data.ToolName;
    var name = data.Name;
    var id = data.Id;
    if (document.getElementById("component-" + toolName) == null) {
        var path = "/components/" + toolName + "/component.html";
        var link = document.createElement('link');
        link.setAttribute('designed', '');
        link.rel = "import";
        link.href = path;
        link.id = "component-" + toolName;
        document.head.appendChild(link);
    }
    var element = document.createElement(name);
    element.setAttribute('designed', '');
    element.className = "element";
    element.id = id;
    var properties = data.Properties;
    properties.forEach(function (v, i, a) {
        try {
            element[v.Name] = v.Value;
        }
        catch (e) {
            console.log(e);
        }
    });
    var loadedAction = function () {
        if (eval(isReady.toLowerCase()) == true) {
            element.createNew();
        }
    };
    var $rootCanvas = $('.root.canvas');
    $rootCanvas.append(element);
    if (element.innerHTML.length < 1) {
        var listener = function () {
            element.removeEventListener('DOMSubtreeModified', listener);
            loadedAction();
            bound.onLoadedComponent(id);
        };
        element.addEventListener('DOMSubtreeModified', listener);
    }
    else {
        loadedAction();
        bound.onLoadedComponent(id);
    }
}
function removeComponent(id) {
    var element = document.getElementById(id);
    $(element).remove();
}
function getProperties(id) {
    var element = document.getElementById(id);
    var result = new Array();
    var properties = element.properties;
    Object.getOwnPropertyNames(properties).forEach(function (name) {
        result.push({
            type: properties[name].type.name,
            name: name,
            value: element[name],
            valueType: properties[name]['valueType'],
            display: properties[name]['display'],
            group: properties[name]['group'],
            editor: properties[name]['editor']
        });
    });
    element.behaviors.forEach(function (v, i, a) {
        var properties = v.properties;
        Object.getOwnPropertyNames(v.properties).forEach(function (name) {
            result.push({
                type: properties[name].type.name,
                name: name,
                value: element[name],
                valueType: properties[name]['valueType'],
                display: properties[name]['display'],
                group: properties[name]['group'],
                editor: properties[name]['editor']
            });
        });
    });
    return result;
}
function applyPropertyValue(target, name, value) {
    var element = document.getElementById(target);
    try {
        element[name] = JSON.parse(value);
    }
    catch (e) {
        try {
            element[name] = value;
        }
        catch (error) { }
    }
    console.log("applyPropertyValue - " + target + ", " + name + " : " + value);
}
function applyStyle(target, name, value) {
    var element = document.querySelector(target);
    try {
        $(element).css(name, JSON.parse(value));
    }
    catch (e) {
        try {
            $(element).css(name, value);
        }
        catch (error) { }
    }
    console.log("applyStyle - " + name + " : " + value);
}
