/// <reference path="../../typings/_references.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Mvs;
(function (Mvs) {
    var Convert = (function () {
        function Convert() {
        }
        Convert.ToPixel = function (value) {
            if (value == 0)
                return '0';
            return isNaN(value) ? value : value + "px";
        };
        return Convert;
    }());
    var ComponentBaseBehavior = (function (_super) {
        __extends(ComponentBaseBehavior, _super);
        function ComponentBaseBehavior() {
            _super.apply(this, arguments);
        }
        ComponentBaseBehavior.prototype.layoutChanged = function (newValue, oldValue) {
            var data = newValue;
            if (data[0] == "{")
                data = JSON.parse(newValue);
            if (data.top != undefined)
                this.customStyle['--top'] = Convert.ToPixel(data.top);
            if (data.left != undefined)
                this.customStyle['--left'] = Convert.ToPixel(data.left);
            if (data.width != undefined)
                this.customStyle['--width'] = Convert.ToPixel(data.width);
            if (data.height != undefined)
                this.customStyle['--height'] = Convert.ToPixel(data.height);
            if (data.margin != undefined)
                this.customStyle['--margin'] = Convert.ToPixel(data.margin);
            if (data.padding != undefined)
                this.customStyle['--root-margin'] = Convert.ToPixel(data.padding);
            this.updateStyles();
        };
        ComponentBaseBehavior.prototype.appearanceChanged = function (newValue, oldValue) {
            var data = newValue;
            if (data[0] == "{")
                data = JSON.parse(newValue);
            if (data.background != undefined)
                this.customStyle['--background'] = data.background;
            this.updateStyles();
        };
        ComponentBaseBehavior.prototype.borderChanged = function (newValue, oldValue) {
            var data = newValue;
            if (data[0] == "{")
                data = JSON.parse(newValue);
            if (data.border != undefined)
                this.customStyle['--border'] = data.border;
            if (data.borderStyle != undefined)
                this.customStyle['--border-style'] = data.borderStyle;
            if (data.borderWidth != undefined)
                this.customStyle['--border-width'] = data.borderWidth;
            if (data.borderRadius != undefined)
                this.customStyle['--border-radius'] = data.borderRadius;
            this.updateStyles();
        };
        ComponentBaseBehavior.prototype.fontSizeChanged = function (newValue, oldValue) {
            var data = newValue;
            if (data[0] == "{")
                data = JSON.parse(newValue);
            if (data.color != undefined)
                this.customStyle['--color'] = data.color;
            if (data.fontFamily != undefined)
                this.customStyle['--font-family'] = data.fontFamily;
            if (data.fontSize != undefined)
                this.customStyle['--font-size'] = data.fontSize;
            if (data.fontStyle != undefined)
                this.customStyle['--font-style'] = data.fontStyle;
            if (data.fontWeight != undefined)
                this.customStyle['--font-weight'] = data.fontWeight;
            this.updateStyles();
        };
        __decorate([
            property({ type: Object, value: { top: 'auto', left: 'auto', width: 'initial', height: 'initial', margin: '0' }, display: "Layout", group: "Layout", valueType: 'Layout' })
        ], ComponentBaseBehavior.prototype, "layout", void 0);
        __decorate([
            observe("layout")
        ], ComponentBaseBehavior.prototype, "layoutChanged", null);
        __decorate([
            property({ type: Object, value: { background: 'transparent' }, display: "Appearance", group: "Appearance", valueType: 'Appearance' })
        ], ComponentBaseBehavior.prototype, "appearance", void 0);
        __decorate([
            observe("appearance")
        ], ComponentBaseBehavior.prototype, "appearanceChanged", null);
        __decorate([
            property({ type: Object, value: { border: 'black', borderStyle: 'solid', borderWidth: '0', borderRadius: 'initial' }, display: "Border", group: "Layout", valueType: 'Border' })
        ], ComponentBaseBehavior.prototype, "border", void 0);
        __decorate([
            observe("border")
        ], ComponentBaseBehavior.prototype, "borderChanged", null);
        __decorate([
            property({ type: Object, value: { fontFamily: 'Sans-serif', fontSize: 'initial', fontStyle: 'normal', fontWeight: 'normal' }, display: "Font", group: "Appearance", valueType: 'Font' })
        ], ComponentBaseBehavior.prototype, "font", void 0);
        __decorate([
            observe("font")
        ], ComponentBaseBehavior.prototype, "fontSizeChanged", null);
        return ComponentBaseBehavior;
    }(polymer.Base));
    Mvs.ComponentBaseBehavior = ComponentBaseBehavior;
    var ComponentBase = (function (_super) {
        __extends(ComponentBase, _super);
        function ComponentBase() {
            _super.apply(this, arguments);
        }
        return ComponentBase;
    }(polymer.Base));
    Mvs.ComponentBase = ComponentBase;
})(Mvs || (Mvs = {}));
